
[Socket.IO](https://socket.io/ "Socket Io Project") enables real-time bidirectional event-based communication. It works on every platform, browser or device, focusing equally on reliability and speed. Socket.IO is built on top of the WebSockets API (Client side) and Node.js.

Socket.IO is not a WebSocket library with fallback options to other realtime protocols. It is a custom realtime transport protocol implementation on top of other realtime protocols. Its protocol negotiation parts cause a client supporting standard WebSocket to not be able to contact a Socket.IO server.
And a Socket.IO implementing client cannot talk to a non-Socket.IO based WebSocket or Long Polling Comet server. Therefore, Socket.IO requires using the Socket.IO libraries on both client and server side.

The official site says,
> Socket.IO enables real-time bidirectional event-based communication.

The Socket.IO enables the client and server to communicate in real time. The Socket.IO Node.Js server defines the events that to be triggered on a specific route.Those events are listened by server which are emitted by clients.Also, a server can emit an event to which a client can listen.

The Socket.IO can easily be implemented over web apps using NodeJs server as a backend. But when it comes the case of client as an Android the ?

![]({{site.baseurl}}/https://i.imgflip.com/w1g40.jpg)

Then we have a best library created by **Naoyuki Kanezawa** ([@nkwaza](https://github.com/nkzawa)).
and one by official [Socket.IO](https://github.com/socketio "Github") for android as client of Socket.IO server. 

It depends on you what do you want to choose as your personal preference.
1. [Socket.Io Android-Java client](https://github.com/socketio/socket.io-client-java) **Or**
2. [Nkwaza Android Socket.IO client library](https://socket.io/blog/native-socket-io-and-android/)

you can use either of them. I would recommend you the **second** one because it's very handy and kinda easy to use.
You can go through the [official blog](https://socket.io/blog/native-socket-io-and-android/) on Socket.IO site by NKwaza himself.

To implement the Socket.IO android library in perfect and flawless way you should read further.
There are three steps for implementing SOcket.IO Android Client library.

1. Initialise the Socket
2. Connect the socket to the server
3. Start emitting events or listening events

Many a times when we use Socket.IO the problems generally faced are 1-Incorrect initialisation and it gives `NullPointerExecption`. 2- The proper event could not be emitted and listened as the socket is not connected.


To avoid this try the following approach.

### 1 Initialising the socket

To intialise the socket create an `<name-of your application>` class which extends `Application` class.
See the code snippet below


Here in the application class, declare a private variable `socket` which is we are going to use as a reference you know.<br>
The helper method `getSocket()` will return the `socket` instance which we can use in our whole project.


### 2 Connecting the socket

In oreder to connect the socket to server we need to use the `connect()` method on the socket instance returned from `getSocket()` method.
Inside your `MainActivity` or any activity, add the following code in `onCreate()` method



### 3 Listen or Emitt event

To emit event you can use `emit()` method.The `emit()` triggers an event on the server which you are going to implement as a button click or any actions response.

To listen specific events sent by server, we use `on()` method. The `on()` method opens a channel provided and recieves the response from server.

You can follow the code below



SOmetimes you nedd to listen the response of certain events which client/app is going to trigger either via button click or any action triggered etc. 
In such cases, we can call the `on()` method over `emit()` method. The `emit()` method will trigger some event on specific route/channel and the `on()` will immediatey listen the response send by the server.
You can achieve this by doing something like



### Data/Payload over request
The request payload or request body data must be send in `JSON format` to socket. The `JSON Object` is the input parameter of `emit()` method. The Java's `JSONObject` and `JSONArray` class helps us to construct a JSON body format of the data.
You can do this as 







