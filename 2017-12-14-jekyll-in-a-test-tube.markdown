---
layout: post
title: Jekyll in a Test Tube for experiment
date: 2017-12-14 22:32:20 +0300
description: Beginning with Jekyll (a static file generator) which is under experiment.
img: https://cdn.netlify.com/473a594f32d61d5eb57c9e76930e3ee3617ee04d/4d198/img/blog/jekyllrb.png
tags: [Jekyll,Web-Designing]
---

The first time I give a kick to Jekyll and what can I say it was Goal !!
I used to read the blogs of awesome people and one thing I found that almost all the awesome blogs are statically generated using either <a style="color:#fff;" href="https://jekyllrb.com/">Jekyll</a> or <a style="color:#fff;" href="https://hyde.github.io/">Hyde</a> or some other tools like **Pelican** and **Hugo** and Hexo etc etc.  

There was **Hyde** in test tube initially, but I found that Hyde supports only python 2.7.X (not above 3.5) and I got mad. Whatt!! I mean why 
![Hyde was thrown from test tube](http://i0.kym-cdn.com/entries/icons/original/000/005/635/63e51038_f4b0_682c.jpg)  

The day I found the Jekyll, It's meh ! Do i need to learn **Ruby**? but surprisingly NO. I started shaking the viscousy red liquid from the test tube. I found a site for themes of Jekyll. There are lot of sites you can just Google it "Jekyll themes" and you will get n number of different themes.  

I selected on and just renovated it and bang on it worked like a charm ! Since the **Jekyll** was under the experiment I thought let's look deeper inside the viscousy liquid (A Programmer's Microscope) ![heavy microscope](https://i.imgflip.com/f16v1.jpg)  
### So basically the results are something like this,
Jekyll is a static site generator and it builds site logically. Your site is served as an object to which we can modify or append whatever we want,  
There are following steps when you hit the `Jekyll build` 

										  reset
                                            |
                                          read
                                          	|
                                          generate
                                          	|
                                          render
                                          	|
                                          cleanup
                                          	|
                                          write
                                          	|
                                          print_stats

As the build process runs a first function is called <code> def process </code> and then each state is followed.
`reset`  
clears the cache and resets some internal variables.  
`read`
This is where the data for your site is read from disk and converted to Jekyll’s internal data structures.

Document :-Blog posts are represented as documents.  

Page:-Represents a page in your site. Markdown or HTML files (containing front matter) that are not blog posts become pages.  

StaticFile:-Any file that does not contain front matter is represented as a static file. Static files are simply copied over to the destination folder and are not processed by the rendering engine. 

ALl this things are performed on your site behaving as an Object and yoy know what n how we can operate an instance.

`generate`  
After the data structures for your site have been created, we move on to the generate step. This step involves looping over all of the generators defined for your site and calling generate on them.

`render`
The documents and pages in your site are processed in the render step. This is arguably the core of the entire process. The jekyll uses <a style="color:#fff;" href="https://shopify.github.io/liquid/">Liquid templating engine</a> render a **markdown (.md)** . This markdown/text format is converted into the **HTML code** which a browser can render easily to show a web page. If the layout parameter for a page or document is defined, then the contents of the document are rendered within that layout.

`cleanup` and `print_stats`
Your generated site is now ready to be written to disk, but before Jekyll can do that it needs to clean up the destination directory. All the existing files in the destination directory that are not part of the current build process are removed at this stage.  
For each file that belongs to the site being generated, its contents are written to disk. Static files are simply copied over to their destination path. Directories are created as needed to match the destination path for each file.  
If you run <code>jekyll build</code> with the <code>— profile option<code>, this step will print out a stats table from the Liquid renderer, which lists the rendering times for each file. 
  
  
So the conclusion is, Jekyll is awesome to generate a static website as it doesn't break the Number 1 rule of software industry > #1.Never Repeat Yourself. On the other hand the dis-advantage is that ... ya It's static. But Jekyll is very best framework we can say to get your small blog-cum-portfolio or small business site to start.

To deploy the Jekyll site you can use <a style="color:#fff;" href="https://www.heroku.com/"> Heroku </a>
or <a style="color:#fff;" href="https://www.github.com/">Github</a> ( **Github** supports Markdowns and statically generated pages ).
I personally used **Heroku** I don't know why. It's just .. I wanted to make a non open source blog xD.
So you can say, "Man!! Do you git push your blog post?", and I reply, "A...yes my child! I do exist!".


I know I breifly touched the internals of Jekyll because by testing under microscope I just ran out of that viscousy red liquid ;-). Anyway, I will be more concentrated on posting Android related posts and just let me know what posts you want related to android as we go!  
Because this is the most awesome Blog in the world  
![Awwwesome](https://img00.deviantart.net/776b/i/2012/286/9/8/it__s_time_to_kick_buttowski__by_kittyloverrr-d5ho08s.png)

